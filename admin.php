<?php
session_start();
if (!$_SESSION['admin']) {
    header("Location: admin_login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Page</title>
</head>

<body>
    <h1>Simple Blog</h1>
    <hr>
    <h3>Admin Page | <a href="logout.php">Logout</a></h3>
</body>

</html>