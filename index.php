<?php
include "conn.php";
$posts = mysqli_query($conn, "SELECT * FROM post");
$post_rows = mysqli_num_rows($posts);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple Blog</title>
</head>

<body>
    <h1>Simple Blog</h1>
    <hr />

    <?php
    if ($post_rows > 0) {
        foreach ($posts as $post) {
            echo "<a href='post.php?id={$post['id']}'><h2>" . $post["title"] . "</h2></a>";
            echo "<small>Date: " . $post['createdAt'] . "</small>";
            echo "<p>";
            echo $post["content"];
            echo "</p>";
            echo "<hr/>";
        }
    } else {
        echo "Data not found";
    }
    ?>
</body>

</html>