<?php
include "conn.php";
$id = $_GET['id'];
$q  = mysqli_query($conn, "SELECT * FROM post WHERE id = {$id}") or die(mysqli_error($conn));
$post = mysqli_fetch_array($q);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog Detail</title>
</head>

<body>
    <h2><?php echo $post['title'] ?></h2>
    <small>Date: <?php echo $post['createdAt'] ?></small>
    <p>
        <?php echo $post['content'] ?>
    </p>
</body>

</html>