<?php
session_start();
include 'conn.php';

if (isset($_POST['submit'])) {
    $user_rows = 0;
    $username = $_POST['username'];
    $password = $_POST['password'];

    // filter input
    // $username = mysqli_real_escape_string($conn, $_POST['username']);
    // $password = mysqli_real_escape_string($conn, $_POST['password']);

    $q = "SELECT * FROM user WHERE username = '{$username}' AND password = '{$password}'";
    $user = mysqli_query($conn, $q);
    if ($user) {
        $user_rows = mysqli_num_rows($user);
    }

    // menggunakan bind parameter
    // $q = mysqli_prepare($conn, "select * from user where username = ? and password = ?");
    // mysqli_stmt_bind_param($q, "ss", $username, $password);
    // mysqli_execute($q);
    // mysqli_store_result($user);
    // if ($user) {
    //     $user_rows = mysqli_num_rows($user);
    // }

    if ($user_rows == 0) {
        die("Wrong username or password!");
    } else {
        $_SESSION['admin'] = 1;
        header("Location: admin.php");
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Login</title>
</head>

<body>
    <h1>Admin Login</h1>
    <hr />
    <form action="admin_login.php" method="POST">
        <table>
            <tr>
                <td>Username</td>
                <td>:</td>
                <td><input type="text" name="username" id="username" size=100></td>
            </tr>
            <tr>
                <td>Password</td>
                <td>:</td>
                <td><input type="text" name="password" id="password" size=100></td>
            </tr>
            <tr>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td><input type="submit" name="submit" id="submit" value="Login"></td>
            </tr>
        </table>
    </form>
</body>

</html>